import javafx.util.converter.DoubleStringConverter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;


public class Sequantial {
    public List<Dense> layers;
    public List<Double> errors;

    public Sequantial() {
        layers = new ArrayList<>();
        errors = new ArrayList<>();
    }

    public void add(Dense layer) {
        layers.add(layer);
    }

    public void compile() {
        layers.get(0).compile(0);
        for (int i = 1; i < layers.size(); i++) {
            layers.get(i).compile(layers.get(i - 1).nodes.length);
        }
    }

    public void printWeights() {
        for (Dense d :
                layers) {
            d.printNodes();
        }
    }

//    public void fit(List<List<Double>> features, double label, int epoch) {
//        for (int i = 0; i < epoch; i++) {
//            List<double[]> predictions = evaluate(features);
//            for (double [] pred:
//                    ) {
//                errors.add(getLoss(y[0], label));
//            }
//        }
//        mse();
//    }

    public List<double[]> evaluate(List<List<Double>> features) {

        List<double[]> labels = new ArrayList<>();
        double[][] features_m = new double[features.size()][features.get(0).size()];
        for (int i = 0; i < features.size(); i++) {
            for (int j = 0; j < features.get(i).size(); j++) {
                features_m[i][j] = features.get(i).get(j);
            }
        }
        for (double[] f :
                features_m) {
            labels.add(evaluate(f));
        }
        calculateDeltas();
        return labels;
    }

    public double[] evaluate(double[] features) {
        double[] x = layers.get(0).evaluate(features);
        for (int i = 1; i < layers.size(); i++) {
            x = layers.get(i).evaluate(x);
        }
        return x;
    }


    private double mse() {
        double sum = 0;
        for (double err :
                errors) {
            sum += err;
        }
        return sum / errors.size();
    }

    private double getLoss(double predicted, double valid) {
        return Math.pow(predicted - valid, 2);
    }

    public void calculateDeltas() {
        layers.get(layers.size() - 1).calculateDeltas(null);
        for (int i = layers.size() - 2; i >= 0; i--) {
            layers.get(i).calculateDeltas(layers.get(i + 1));
        }
    }

    public void printDeltas() {
        for (Dense layer : layers) {
            layer.printDerivates();
        }
    }


}
