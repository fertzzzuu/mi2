import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Node {
    public List<Double> weights;
    public double delta;

    public double bias;
    public double s;
    public double y;
    public List<Double> wDerivative;

    public Node(){
        weights = new ArrayList<>();
        wDerivative = new ArrayList<>();
        bias = 0;

    }


    public void setBias(double bias) {
        this.bias = bias;
    }

    public void setWeightsAndBias(int n){
        Random rnd = new Random();
        for (int i = 0; i < n; i++) {
            weights.add(rnd.nextGaussian()*0.1);
        }
    }

    public void setWeightsAndBias(List<Double> weights){
        List<Double> x= new ArrayList<>(weights.subList(0,weights.size()-1));
        this.weights = x;
        this.bias = weights.get(weights.size()-1);
    }


    public void printWeightsAndBias() {
        for (Double w:
             weights) {
            System.out.print(w);
            System.out.print(',');
        }
        System.out.println(bias);


    }

    public double evaluate(double[] feature, String activation) {
        double sum = 0;
        for (int i = 0; i < weights.size(); i++) {
            sum += (weights.get(i)*feature[i]);
        }
        this.s = sum + bias;

        switch (activation) {
            case "relu":
                y = Math.max(sum+bias, 0);
                return y;
            case "linear":
                y = sum + bias;
                return y;
            default:
                return 0;
        }
    }

    public void calculateDelta() {
        delta = 1;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public double getWDerivative(double f){
        this.wDerivative.add(delta*f);
        return delta*f;
    }

    public double getBerivate() {
        return delta;
    }
}
