import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NNSolutionOne {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        String[] nums = line.split(",");
        List<Integer> layers = new ArrayList<>();
        for (String num :
                nums) {
            layers.add(Integer.parseInt(num));
        }


        Sequantial model = new Sequantial();
        model.add(new Dense(layers.get(1), layers.get(0)));
        for (int i = 2; i < layers.size(); i++) {
            model.add(new Dense(layers.get(i)));
        }

        model.compile();

        for (int i = 0; i < layers.size() - 1; i++) {
            System.out.print(layers.get(i));
            System.out.print(',');
        }
        System.out.println(layers.get(layers.size()-1));
        model.printWeights();



    }
}
