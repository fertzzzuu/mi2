import java.util.List;

public class Dense {
    public String activation;
    public Node[] nodes;
    public int input_dim;
    public double[] feature;

    public Dense(int n) {
        nodes = new Node[n];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node();
        }
    }

    public Dense(int n, int input_dim) {
        this(n);
        this.input_dim = input_dim;
    }

    public Dense(int n, int input_dim, String activation) {
        this(n, input_dim);
        this.activation = activation;
    }

    public Dense(int n, int input_dim, List<List<Double>> weights) {
        this(n, input_dim);
        setWeightsOfNodes(weights);
    }

    public Dense(int n, List<List<Double>> weights) {
        this(n);
        setWeightsOfNodes(weights);
    }

    public void setWeightsOfNodes(List<List<Double>> weights) {
        if (weights.size() == nodes.length) {
            for (int i = 0; i < weights.size(); i++) {
                nodes[i].setWeightsAndBias(weights.get(i));
            }
        }
    }


    public void compile(int length) {
        if (length != 0)
            for (Node n : nodes)
                n.setWeightsAndBias(length);
        else
            for (Node n : nodes)
                n.setWeightsAndBias(input_dim);
    }

    public void printNodes() {
        for (Node n :
                nodes) {
            n.printWeightsAndBias();
        }
    }

    public double[] evaluate(double[] feature) {
        this.feature = feature;
        double[] labels = new double[nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            labels[i] = nodes[i].evaluate(feature, activation);
        }

        return labels;
    }

    public void calculateDeltas(Dense nextLayer) {
        if (nextLayer == null) {
            for (int i = 0; i < nodes.length; i++) {
                nodes[i].calculateDelta();
            }
        } else {
            for (int i = 0; i < nodes.length; i++) {
                double sum = 0;
                for (int j = 0; j < nextLayer.nodes.length; j++) {
                    sum += nextLayer.nodes[j].delta * nextLayer.nodes[j].weights.get(i);
                }
                nodes[i].setDelta(sum * getReLUDerivate(nodes[i].s));
            }
        }
    }


    private double getReLUDerivate(double s) {
        return s > 0 ? 1 : 0;

    }


    public void printDerivates() {
        for (Node node : nodes) {
            for (double f : feature) {
                System.out.print(node.getWDerivative(f) + ",");
            }
            System.out.println(node.getBerivate());
        }
    }
}
