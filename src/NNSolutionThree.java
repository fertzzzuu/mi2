import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class NNSolutionThree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        String[] nums = line.split(",");
        List<Integer> layers = new ArrayList<>();
        for (String num :
                nums) {
            layers.add(Integer.parseInt(num));
        }

        Sequantial model = new Sequantial();
        if (layers.size() > 2) {
            model.add(new Dense(layers.get(1), layers.get(0), "relu"));

            for (int i = 2; i < layers.size() - 1; i++) {
                model.add(new Dense(layers.get(i), 0, "relu"));
            }
            model.add(new Dense(layers.get(layers.size() - 1), 0, "linear"));
        } else
            model.add(new Dense(layers.get(1), layers.get(0), "linear"));
        for (int i = 1; i < layers.size(); i++) {
            List<List<Double>> weights = new ArrayList<>();
            for (int j = 0; j < layers.get(i); j++) {
                line = sc.nextLine();
                nums = line.split(",");
                List<Double> igen = new ArrayList<>();
                for (String num : nums)
                    igen.add(Double.parseDouble(num));
                weights.add(igen);
            }
            model.layers.get(i - 1).setWeightsOfNodes(weights);
        }

        int input_num = Integer.parseInt(sc.nextLine());

        List<List<Double>> inputs = new ArrayList<>();
        for (int i = 0; i < input_num; i++) {
            line = sc.nextLine();
            nums = line.split(",");
            List<Double> igen = new ArrayList<>();
            for (String num : nums)
                igen.add(Double.parseDouble(num));
            inputs.add(igen);
        }
        for (int i = 0; i < layers.size() - 1; i++) {
            System.out.print(layers.get(i));
            System.out.print(',');
        }
        System.out.println(layers.get(layers.size()-1));
        List<double[]> y = model.evaluate(inputs);
        model.printDeltas();
//        for (double[] asd :
//                y) {
//            for (int i = 0; i < asd.length - 1; i++) {
//                System.out.print(asd[i]);
//                System.out.print(',');
//            }
//            System.out.println(asd[asd.length - 1]);
//        }

    }
}
